#!/bin/bash:
export HERE=/home/orca/daltonks/2020_projects/umwmbug_fix/ww3/ww3v6.07/model
export NETCDF=/home/disk/orca2/mcurcic/opt/netcdf-4.4_intel-17.0.4

export PATH=$PATH:$HERE
export PATH=$PATH:$HERE/bin
export PATH=$PATH:$HERE/exe
export WWATCH3_NETCDF=NC4
export NETCDF_CONFIG=$NETCDF/bin/nc-config

# source /opt/intel/compilers_and_libraries_2019/linux/bin/compilervars.sh intel64
source /opt/intel/compilers_and_libraries_2018.5.274/linux/bin/compilervars.sh intel64

export set I_MPI_SHM_LMT=shm
ulimit -s unlimited
