import os
import subprocess as sub
import logging
# def call_system(string)
#  try:
#      subprocess.call([string])
#  except OSError:
#      print ('wrongcommand does not exist')

def dictionaries_versions(version):
    if version == '20200203':
        bound_dir = {
        #'saopaulo': f'input/boundary/saopaulo'
                      }

        forca_dir = {'wind'    : f'input/forcing/wind'}

        grid_dir = {'irene'  : f'input/grid/irene',
                    'points'  : f'input/grid/points'}


        output_dir = {'irene'  : f'output/irene',
                       'points' : f'output/points',}

        restrt_dir = {'irene'   : f'restart/irene',
                       }

        run_dir = {'irene' : f'run'}

        varmap = {'wnd20200203': 'wind',
                  'ice20200203': 'ice'}

        directories = {'grid': grid_dir,
                       'forc': forca_dir,
                       'bound_dir': bound_dir,
                       'output_dir': output_dir,
                       'restart_dir': restrt_dir,
                       'run_dir': run_dir,
                       'varmap': varmap}

    else:
        raise IOError('verion needs to be set')

    return directories


def execute_shell(instring):
    success = sub.call(instring, shell=True)
    if success == 1:
        raise OSError(f'something went wrong while executing: {instring}')
    else:
        logging.info(f'successfully executed: {instring}')


def copy_stuff(filein, fileout):
    comm = f'cp {filein} {fileout}'
    execute_shell(comm)


def rm_stuff(filein):
    comm = f'rm {filein}'
    execute_shell(comm)


def mkdir_stuff(filein):
    if (os.path.exists(filein)):
        logging.info(f'{filein} already exists!')
    comm = f'mkdir {filein}'
    execute_shell(comm)


def chdir_stuff(dirin):
    comm = f'cd {dirin}'
    execute_shell(comm)



def run_stuff(comsstr):
    execute_shell(comsstr)
