import os
from . import _ww3_directory_structure as wds
import pathlib
import subprocess as sub
import datetime as dtt
import numpy as np
import logging

def shel_run(infile='ww3_shel.substitute',
             outfile="ww3_shel.inp",
             time0=None,
             delta_time=None,
             dt_outp=3600,
             dt_trck=0,
             dt_rstr=86400,
             dt_bndy=0,
             dt_spar=0,
             version='20200203',
             **kwargs):
    """
    shel_run copies the input data and auxilliary files into the simulation
    directory.


    Parameters
    ----------
    infile : string
        name of the template (default: ww3_shell.substitute)
    outfile : string
        output .inp name (defauly: ww3_shel.inp)
    time0 : datetime.datetime
        initial time
    delta_time : datetime.timedelta
        interval of time used for the simulation - datetime.timedelta(days=3),
        for instance
    dt_outp : int
        output interval in seconds
    dt_trck : int
        track interval in seconds
    dt_rstr : int
        restart interval in seconds
    dt_bndy : int
        boundary interval in seconds
    dt_spar : int
        wave separtion of wave field in seconds
    version: str
        set up a version for the paths (see _ww3_directory_structure.py)
    **kwargs : dict
        dummy dictionary
    """

    path = pathlib.Path().absolute()  # use current path
    d = wds.dictionaries_versions(version)  # get dictionaries with paths

    # set initial and final time strings
    time0_str = time0.strftime('%Y%m%d %H%M%S')
    timef_str = (time0 + delta_time).strftime('%Y%m%d %H%M%S')

    # get directories paths
    run_dir = d['run_dir']['irene']
    inp = d['grid']
    frc = d['forc']

    # copying tot run directory
    for grid in ['irene']:
        wds.copy_stuff(f'{path}/{inp[grid]}/mod_def.ww3',
                       f'{path}/{run_dir}/mod_def.ww3')

    for forc in ['wind']:
        wds.copy_stuff(f'{path}/{frc[forc]}/{forc}.ww3',
                       f'{path}/{run_dir}/{forc}.ww3')

    # set up values that will be used in ww3_shel.inp.substitute
    sinp = {"%time0"   : f"{time0_str}",
            "%timef"   : f"{timef_str}",
            "%output"  : f"{time0_str} {dt_outp} {timef_str}",
            "%track"   : f"{time0_str} {dt_trck} {timef_str}",
            "%restart" : f"{time0_str} {dt_rstr} {timef_str}",
            "%boundary": f"{time0_str} {dt_bndy} {timef_str}",
            "%separate": f"{time0_str} {dt_spar} {timef_str}"}


    # reading template
    fopen = open(f'{run_dir}/original/{infile}', 'rt')
    data = fopen.read()

    #replacing values
    for k in sinp.keys():
        data = data.replace(k, sinp[k])
    fopen.close()

    # writing output .inp
    fin = open(f'{run_dir}/ww3_shel.inp', "wt")
    fin.write(data)
    fin.close()


# output
def main_output(time0=None, version=None, **kwargs):
    """
    Sets up the data and auxilliary ww3_ounf.inp to output information in
    netcdf

    Parameters
    ----------
    time0 : datetime.datetime
        Initial time
    version : string
        set up a version for the paths (see _ww3_directory_structure.py)
    **kwargs : dict
        dummy dictionary

    """
    path = pathlib.Path().absolute()  # use current path
    d = wds.dictionaries_versions(version)  # getting paths
    run = d['run_dir']['irene']   # set run path

    datestr = time0.strftime('%Y%m%d')  # get time as string

    # prepare output directory for ww3_ounf
    for grid in ['irene']:
        outp = d['output_dir'][grid]  # get output path
        wds.mkdir_stuff(f'{path}/{outp}/{datestr}')  # crate new path

        # copy output data to output directory
        wds.copy_stuff(f'{run}/mod_def.ww3',
                       f'{path}/{outp}/{datestr}/mod_def.ww3')

        wds.copy_stuff(f'{run}/out_grd.ww3',
                       f'{path}/{outp}/{datestr}/out_grd.ww3')

        # replace values
        ounf_inp(time0=time0,
                 infile=f'{outp}/original/ww3_ounf.inp.substitute',
                 outfile=f'{outp}/{datestr}/ww3_ounf.inp')


def ounf_inp(infile='ww3_ounf.inp.substitute',
             outfile='ww3_ounf.inp',
             time0=None,
             dt_main=3600,
             n=9999):
    """
    Replace time values in the template of ww3_ounf.inp

    Parameters
    ----------
    infile : str
        name of the template (default: ww3_ounf.inp.substitute)
    outfile : str
        output .inp name (defauly: ww3_ounf.inp)
    time0 : datetime.datetime
        Description of parameter `time0`.
    dt_main : type
        Description of parameter `dt_main`.
    n : type
        Description of parameter `n`.

    Returns
    -------
    type
        Description of returned object.

    """
    # initial time string
    time0_str = time0.strftime('%Y%m%d %H%M%S')

    # values to be replaced in the template
    ounf_inp = {"%main": f"{time0_str} {dt_main} {n}"}

    # open template
    fopen = open(infile, 'rt')
    data = fopen.read()

    # replace values in string
    for k in ounf_inp.keys():
        data = data.replace(k, ounf_inp[k])
    fopen.close()

    # write outfile 
    fin = open(outfile, "wt")
    fin.write(data)
    fin.close()
