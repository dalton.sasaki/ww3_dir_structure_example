import numpy as np
import datetime as dtt
from scripts.model import multi_ww3 as multi
from scripts.model import shel_ww3 as shel
from scripts.retrieve import manage as mng
import os
import pathlib
import pandas as pd

def raiseBashError(fun, message=None):
    try:
        a = fun

    except BaseException as e:
        logger.error('Failed to do something: ' + str(e))

fdppath = ':/path/to/ww3/executables'
os.environ['PATH'] +=fdppath
os.system('echo $PATH')

# raiseBashError(os.system('ww3_grid'))  # test if path is ok

path = pathlib.Path().absolute()

time0 = dtt.datetime(2011, 8, 23, 12)
dtime = dtt.timedelta(days=3)
tlist = pd.date_range(time0, time0+dtime, freq='1H')

# organizing time parameters ingested by ww3_shel.inp.substitute
kwargs = {'time0': time0,
               'delta_time': dtime,
               'version': '20200203'}


##############################################################################
## Spatial grid & forcing                                                    #
##############################################################################
# prepare grids in input/grid
multi.multi_grid(version='20200203', grids=['irene', 'points'])

# copy auxiliary files and run grids executables
bash_command=f"cd {path}/input/grid/irene &&" + \
             f'ww3_grid &&' + \
             f"cd {path}/input/grid/points &&" + \
             f'ww3_grid'
raiseBashError(os.system(f'{bash_command}'))

# prepare forcing grids in input/forcing
bash_command=f"cd {path}/input/forcing/wind/&&" + \
             f'cp original/ww3_prnc.inp . &&' + \
             f'cp original/ww3_grid.inp . &&' + \
             f"cd {path}"
raiseBashError(os.system(f'{bash_command}'))


# bash instructions
bash_command=f"cd {path}/input/forcing/wind/&&" + \
             f'ww3_grid &&' + \
             f"cd {path}"
raiseBashError(os.system(f'{bash_command}'))

# compile forcing grid and forcing input
bash_command=f"cd {path}/input/forcing/wind/&&" + \
             f'ww3_prnc &&' + \
             f"cd {path}"
raiseBashError(os.system(f'{bash_command}'))


##############################################################################
## Model run                                                                 #
##############################################################################

# prepare ww3_shel.inp
shel.shel_run(**kwargs)

# prepare outptut poinst
multi.main_output(dtt=time0, version='20200203')
