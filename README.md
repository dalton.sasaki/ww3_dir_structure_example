# Wavewatch3 simple data structure

This package organizes the simulation directories and partially  encapsulates ww3 auxilliary files (.inp) in order to run a wave simulation of Hurricane Irene. It combines bash scripts (for simplicity) and python scripts (for flexibility and readability).


`main.py` controls the execution ww3 programs, such as ww3_grid (grid), ww3_prnc (forcing), ww3_shel (single grid model), etc. It configuration is related to time parameters within ww3_multi.inp, ww3_shel.inp and ww3_ounf.inp, such as period of simulation, restart times, output times. Directories named `original` contain the templates of the files used by `main.py`.


**Notice that this code is presented only as a simplified example for a hindcast simulation. Although the model grid (irene/original) and .inp files are functional, the scripts won't work without a proper compilation of the ww3 model, input data and linking**


## Directory structure
The data directories are organized as follows:
* scripts contains all the python/bash scripts used to prepare the simulation
* the other directories are used for preprocessing/running/postprocessing of ww3 files

```
.
├── envs
├── input
│   ├── forcing
│   │   └── wind
│   │       └── original
│   └── grid
│       ├── irene
│       │   └── original
│       └── points
│           └── original
├── model
├── output
│   ├── irene
│   │   └── original
│   └── original
├── restart
│   └── irene
├── run
│   └── original
└── scripts
    └── model
```
